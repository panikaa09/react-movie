import React from "react";

import {TitlePage, ContactsInfo, ContactsMap, ContactsForm} from "../components";

const Contacts = () => {
  return (
    <React.Fragment>
      <TitlePage title='контактная информация' subtitle='контакты'/>
      <ContactsInfo/>
      <ContactsMap/>
      <ContactsForm/>
    </React.Fragment>
  )
};

export default Contacts;