import React from "react";

import {TitlePage, FaqInfo} from "../components";

const Faq = () => {
  return (
    <React.Fragment>
      <TitlePage title='часто задаваемые вопросы' subtitle='faq'/>
      <FaqInfo/>
    </React.Fragment>
  )
};

export default Faq;