import React from "react";

import {AboutInfo, FactInfo, Team, TitlePage} from "../components";

const About = () => {
  return (
    <React.Fragment>
      <TitlePage title='кто мы' subtitle='о нас'/>
      <AboutInfo/>
      <FactInfo/>
      <Team/>
    </React.Fragment>
  )
};

export default About;