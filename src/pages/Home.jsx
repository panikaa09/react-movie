import React from "react";

import { HomeSlider, Filter, ComingSoon, TopBuy, NewsLetter } from "../components";

const Home = () => {
  return (
    <React.Fragment>
      <HomeSlider />
      <Filter />
      <ComingSoon />
      <TopBuy />
      <NewsLetter />
    </React.Fragment>
  )
};

export default Home;