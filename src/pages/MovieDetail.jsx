import React from "react";
import {useSelector} from "react-redux";

import {TopBuy, MovieDetailItem, TitlePage} from "../components";

const MovieDetail = () => {
  const movieDetailInfo = useSelector(({movieDetail}) => movieDetail.movieDetail);

  return (
    <React.Fragment>
      <TitlePage title='факты и информация' subtitle='детально о фильме'/>
      <MovieDetailItem detail={movieDetailInfo}/>
      <TopBuy/>
    </React.Fragment>
  )
};

export default MovieDetail;