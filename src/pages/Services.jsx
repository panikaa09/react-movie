import React from "react";

import {ServicesInfo, HowItWorkInfo, GenresInfo, TitlePage} from "../components";


const Services = () => {
  return (
    <React.Fragment>
      <TitlePage title='услуги которые мы предлагаем' subtitle='услуги'/>
      <ServicesInfo/>
      <HowItWorkInfo/>
      <GenresInfo/>
    </React.Fragment>
  )
};

export default Services;