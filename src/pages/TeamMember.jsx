import React from "react";

import {TitlePage, TeamMemberInfo, TeamMemberSkills} from "../components";

const TeamMember = () => {
  return (
    <React.Fragment>
      <TitlePage title='наша команда' subtitle='встречайте наших людей'/>
      <TeamMemberInfo />
      <TeamMemberSkills />
    </React.Fragment>
  )
};

export default TeamMember;