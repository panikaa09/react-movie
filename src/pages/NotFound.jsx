import React from "react";
import {Link} from "react-router-dom";

const NotFound = () => {
  return (
    <section className="section-not-found section-first-content">
      <div className="not-found container">
        <div className="page-title__wrap">
          <div className="page-title">404</div>
          <div className="page-title__sub">
            Страница не найдена
          </div>
        </div>
        <div className="not-found__subtitle">
          Запрошенная страница не может быть найдена - это может быть связано с орфографической ошибкой <br />в ​​URL-адресе или удаленной страницей.
        </div>
        <div className="not-found__link">
          <Link to='/' className='btn-red-filled'>На главную</Link>
        </div>
      </div>
    </section>
  )
};

export default NotFound;