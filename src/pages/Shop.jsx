import React from "react";
import {Filter, TitlePage} from "../components";

const Shop = () => {
  return (
    <React.Fragment>
      <TitlePage title='наслаждайтесь вашими любимыми фильмами' subtitle='магазин'/>
      <section className="section-shop">
        <div className="shop container">
          <Filter />
        </div>
      </section>
    </React.Fragment>
  )
};

export default Shop;