import React from "react";
import {useSelector} from "react-redux";

import {CartEmpty, CartItems, TitlePage} from "../components";

const Cart = () => {
  const cartCount = useSelector(({cart}) => cart.items.length);

  return (
    <React.Fragment>
      <TitlePage title='наслаждайтесь вашими любимыми фильмами' subtitle='корзина'/>
      <section className="section-cart">
        <div className="cart container">
          {
            cartCount && cartCount > 0
            ? < CartItems />
            : <CartEmpty/>
          }
        </div>
      </section>
    </React.Fragment>
  )
};

export default Cart;