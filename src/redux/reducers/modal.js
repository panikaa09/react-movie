const initialState = {
  showModal: false,
  urlTrailer: ''
};

const modal = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_MODAL':
      return {
        ...state,
        urlTrailer: action.payload,
        showModal: true
      };

    case 'SET_SHOW':
      return {
        ...state,
        showModal: false,
      };

    default:
      return state;
  }
};


const initialStateSuccess = {
  showModal: false,
  text: ''
};

const modalSuccess = (state = initialStateSuccess, action) => {
  switch (action.type) {
    case 'SET_SHOW_SUCCESS':
      return {
        ...state,
        showModal: true,
        text: action.payload,
      };

    case 'HIDE_SUCCESS':
      return {
        ...state,
        showModal: false,
      };

    default:
      return state;
  }
};

export {
  modal,
  modalSuccess
};