import { combineReducers } from 'redux';

import {movies, moviesFilter} from './movies';
import {modal, modalSuccess} from './modal';
import filters from "./filters";
import movieDetail from "./movei-detail";
import cart from "./cart";
import teamMember from "./team-member";

const rootReducer = combineReducers({
  movies,
  moviesFilter,
  modal,
  modalSuccess,
  filters,
  movieDetail,
  cart,
  teamMember
});

export default rootReducer;