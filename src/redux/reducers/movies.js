
const initialState = {
  items: [],
  isLoaded: false,
};

const movies = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_MOVIES':
      return {
        ...state,
        items: action.payload,
        isLoaded: true,
      };

    case 'SET_LOADED':
      return {
        ...state,
        isLoaded: action.payload,
      };

    default:
      return state;
  }
};

const initialStateFilter = {
  items: [],
  isLoaded: false,
};

const moviesFilter = (state = initialStateFilter, action) => {
  switch (action.type) {
    case 'SET_MOVIES_FILTER':
      return {
        ...state,
        items: action.payload,
        isLoaded: true,
      };

    case 'SET_LOADED_FILTER':
      return {
        ...state,
        isLoaded: action.payload,
      };

    default:
      return state;
  }
};

export {
  movies,
  moviesFilter
};