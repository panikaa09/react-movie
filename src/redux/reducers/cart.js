import produce from "immer";

const initialState = {
  items: [],
  totalPrice: 0,
  totalCount: 0
};

const getTotalPrice = arr => arr.reduce((sum, obj) => obj.price + sum, 0);

const cart = (state = initialState, action) => {
  return produce(state, draft => {
    switch (action.type) {
      case 'ADD_MOVIE': {

        const newItems = [...draft.items, action.payload];

        return {
          ...state,
          items: newItems,
          totalPrice: getTotalPrice(newItems),
          totalCount: newItems.length
        };
      }

      case 'REMOVE_MOVIE': {

        const newItems = [...draft.items].filter(item => item.id !== action.payload);

        return {
          ...state,
          items: newItems,
          totalPrice: getTotalPrice(newItems),
          totalCount: newItems.length
        };
      }


      default:
        return state
    }
  });
};

export default cart;