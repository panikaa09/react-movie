const initialState = {
  movieDetail: {
    id: null,
    imgUrl: '',
    imgBigUrl: '',
    name: '',
    rateKinoPoisk: null,
    rateImdb: null,
    rateStars: null,
    trailer: '',
    director: '',
    screenwriter: '',
    cast: '',
    desc: '',
    time: '',
    price: null,
    descText: '',
  }
};

const movieDetail = (state = initialState, action) => {
  if (action.type === 'SET_MOVIE_DETAIL') {
    return {
      ...state,
      movieDetail: action.payload,
    };
  }
  return state;
};

export default movieDetail;