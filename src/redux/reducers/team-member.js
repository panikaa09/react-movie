const initialState = {
  teamMember: {
    id: null,
    title: '',
    avatar: '',
    name: '',
    position: '',
    desc: '',
    fullDesc: '',
    skills: []
  }
};

const teamMember = (state = initialState, action) => {
  if (action.type === 'SET_TEAM_MEMBER') {
    return {
      ...state,
      teamMember: action.payload,
    };
  }
  return state;
};

export default teamMember;