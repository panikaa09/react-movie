export const addMovie = (movieObj) => ({
  type: 'ADD_MOVIE',
  payload: movieObj
});

export const removeMovie = (id) => ({
  type: 'REMOVE_MOVIE',
  payload: id
});