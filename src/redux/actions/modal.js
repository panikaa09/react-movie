export const setModal = (url) => ({
    type: 'SET_MODAL',
    payload: url,
});

export const hideModal = () => ({
  type: 'SET_SHOW'
});

export const setShowSuccess = (text) => ({
  type: 'SET_SHOW_SUCCESS',
  payload: text
});

export const hideModalSuccess = () => ({
  type: 'HIDE_SUCCESS'
});