import axios from 'axios';

export const setLoaded = (payload) => ({
  type: 'SET_LOADED',
  payload,
});

export const fetchMovies = (sortBy) => (dispatch) => {
  dispatch({
    type: 'SET_LOADED',
    payload: false,
  });

  axios
    .get(
      `https://db-react-movie.herokuapp.com/movies`,
    )
    .then(({ data }) => {
      dispatch(setMovies(data));
    });
};

export const fetchMoviesFilter = (sortBy) => (dispatch) => {
  dispatch({
    type: 'SET_LOADED_FILTER',
    payload: false,
  });

  axios
    .get(
      `https://db-react-movie.herokuapp.com/movies?_sort=${sortBy.type}&_order=${sortBy.order}`,
    )
    .then(({ data }) => {
      dispatch(setMoviesFilter(data));
    });
};

export const setMovies = (items) => ({
  type: 'SET_MOVIES',
  payload: items,
});

export const setMoviesFilter = (items) => ({
  type: 'SET_MOVIES_FILTER',
  payload: items,
});