export const setTeamMember = (teamMember) => ({
  type: 'SET_TEAM_MEMBER',
  payload: teamMember,
});