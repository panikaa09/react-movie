export const setMovieDetail = (movieDetail) => ({
  type: 'SET_MOVIE_DETAIL',
  payload: movieDetail,
});