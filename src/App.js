import React from 'react';
import {Redirect} from 'react-router';
import {Route, Switch} from 'react-router-dom';

import {Home, Services, Shop, Cart, MovieDetail, NotFound, Contacts, About, Faq, TeamMember} from './pages';

import {Header, Footer, ModalTrailer, ModalSuccess, ScrollToTop} from "./components";
import {useDispatch, useSelector} from "react-redux";
import {fetchMovies} from "./redux/actions/movies";
import {hideModal, hideModalSuccess} from "./redux/actions/modal";

function App() {
  const dispatch = useDispatch();
  const showModal = useSelector(({modal}) => modal.showModal);
  const urlTrailer = useSelector(({modal}) => modal.urlTrailer);
  const showSuccess = useSelector(({modalSuccess}) => modalSuccess.showModal);
  const successText = useSelector(({modalSuccess}) => modalSuccess.text);
  const sortBy = useSelector(({filters}) => filters.sortBy);

  React.useEffect(() => {
    dispatch(fetchMovies(sortBy));
  });


  React.useEffect(() => {
    const modal = document.querySelector('.modal');
    if (modal) {
      modal.addEventListener('click', function (event) {
        dispatch(hideModal());
        document.body.style.overflow = '';
      });
    }
  });

  React.useEffect(() => {
    const modal = document.querySelector('.modal-success');
    modal.addEventListener('click', () => {
      dispatch(hideModalSuccess());
      document.body.style.overflow = '';
    });
  });


  return (
    <div className="App">
      <Header/>
      <main>
        <Route>
          <ScrollToTop/>
          <Switch>
            <Route path="/" component={Home} exact/>
            <Route path="/info/services" component={Services} exact />
            <Route path="/info/about" component={About} exact />
            <Route path="/info/faq" component={Faq} exact />
            <Route path="/info" render={() => <Redirect to="/info/services" />}  exact />
            <Route path="/shop" component={Shop} />
            <Route path="/contacts" component={Contacts} />
            <Route path="/cart" component={Cart} />
            <Route path="/movie-detail" component={MovieDetail} />
            <Route path="/team-member" component={TeamMember} />
            <Route component={NotFound}/>
          </Switch>
        </Route>
      </main>
      <Footer/>
      <ModalTrailer showModal={showModal} urlTrailer={urlTrailer}/>
      <ModalSuccess showModal={showSuccess} text={successText}/>
    </div>
  );
}

export default App;
