import React from "react";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";

import {setMovieDetail} from "../../redux/actions/movieDetail";
import {Stars} from "../index";
import {addMovie, removeMovie} from "../../redux/actions/cart";

const TopBuyItem = (obj) => {
  const dispatch = useDispatch();
  const items = useSelector(({cart}) => cart.items);
  const {imgUrl, name, desc, price, rateStars} = obj;
  const objId = obj.id;

  const id = items.map(item => {
    return item.id
  });

  const changeMovieDetail = () => {
    dispatch(setMovieDetail(obj));
  };

  const onAddMovie = () => {
    dispatch(addMovie(obj));
  };

  const onRemoveMovie = () => {
    dispatch(removeMovie(obj.id));
  };

  return (
    <div className="top-buy__item">
      <Link to='movie-detail' onClick={changeMovieDetail}>
        <div className="top-buy__item-img">
          <img src={imgUrl} alt=""/>
        </div>
      </Link>
      <div className="top-buy__item-info">
        <div className="top-buy__item-name">
          {name}
        </div>
        <div className="top-buy__item-desc">
          {desc}
        </div>
        <div className="top-buy__item-rate">
          <Stars count={rateStars}/>
        </div>
        <div className="top-buy__item-buy">
          <div className="top-buy__item-buy--cash">
            {price}руб.
          </div>
          {
            id.indexOf(objId) === -1
              ? < div className="top-buy__item-buy--cart" onClick={onAddMovie}>
                  <i className="fas fa-shopping-cart"></i>
                </div>
              : <div className="top-buy__item-buy--cart" onClick={onRemoveMovie}>
                  <i className="fas fa-trash"></i>
                </div>
          }
        </div>
      </div>
    </div>
  )
};

export default TopBuyItem;