import React from "react";
import classNames from 'classnames';

const Stars = ({count}) => {
  const starsElements = (() => {
    const result = [];
    for (let i = 0; i <= 5; ++i) {
      result.push(
        <i
          className={
            classNames(
              {'fa fa-star': i < count},
              {'far fa-star': i > count}
            )
          }
          key={i}
        />
      );
    }
    return result;
  })();

  return (
    <div>
      {starsElements}
    </div>
  );
};

export default Stars;