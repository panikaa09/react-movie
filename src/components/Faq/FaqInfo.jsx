import React from "react";

import {FaqItem} from "../index";

const FaqInfo = () => {
  const faqItems = [
    {
      id: 0,
      title: 'How can I change something in my order?',
      text: 'Nisl tincidunt eget nullam non nisi est sit amet facilisis. Lectus proin nibh nisl condimentum id.'
    },
    {
      id: 1,
      title: 'How can I pay for my order?',
      text: 'Quam elementum pulvinar etiam non quam lacus suspendisse. A erat nam at lectus urna duis. Condimentum id venenatis a condimentum vitae sapien pellentesque.'
    },
    {
      id: 2,
      title: 'Can I track my order?',
      text: 'Non quam lacus suspendisse faucibus interdum posuere lorem ipsum dolor. Odio pellentesque diam volutpat commodo sed egestas.'
    },
    {
      id: 3,
      title: 'How long will my order take to be delivered?',
      text: 'Nec sagittis aliquam malesuada bibendum arcu vitae. Massa tempor nec feugiat nisl pretium fusce id. Proin sagittis nisl rhoncus mattis rhoncus urna neque viverra justo.'
    }
  ];

  return (
    <section className="section-faq">
      <h2 className="h2-grey">общие вопросы</h2>
      <div className="faq container">
        <div className="faq__subtitle">
          На этой странице мы собрали самые популярные вопросы, чтобы вы могли легко найти ответ практически на любой свой
          вопрос.
        </div>
        {
          faqItems.map(obj => (
            <FaqItem {...obj} key={obj.id} />
          ))
        }
      </div>
    </section>
  )
};

export default FaqInfo;