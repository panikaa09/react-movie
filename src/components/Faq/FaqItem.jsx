import React from "react";
import Collapsible from "react-collapsible";

const FaqItem = ({title, text}) => {
  return (
    <Collapsible trigger={title}>
      {text}
    </Collapsible>
  )
};

export default FaqItem;