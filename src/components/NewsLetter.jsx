import React from "react";
import {useDispatch} from 'react-redux';
import {useForm} from 'react-hook-form';

import {setShowSuccess} from "../redux/actions/modal";
import Button from "./Button";

const NewsLetter = () => {
  const dispatch = useDispatch();
  const {register, handleSubmit, errors} = useForm();
  const onSubmit = (data) => {
    console.log(data);
    dispatch(setShowSuccess('Рассылка подключена'));
    document.body.style.overflow = 'hidden';
  };

  return (
    <section className="section-news-letter">
      <h2 className="h2-red">новостная рассылка</h2>
      <div className="news-letter container">
        <div className="news-letter__desc">
          Введите адрес электронной почты, чтобы получать все новости, <br/>
          обновления о новых поступлениях, специальные предложения и другую информацию о скидках.
        </div>

        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="news-letter__form-content">
            <div className='form-row'>
              <input name='email' placeholder='Введите email' ref={register({
                required: "Required",
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                  message: "invalid email address"
                }
              })}
              style={{borderColor: errors.email ? '#d81a24' : '', borderWidth: errors.email ? '1px 1px 1px 10px' : ''}}
              />
            </div>
            <Button text='Отправить' cls='btn-red-filled' type='submit'/>
          </div>
        </form>
      </div>
    </section>
  )
};

export default NewsLetter;