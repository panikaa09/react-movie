import React from "react";

import logo from '../assets/images/logo.png';

const Footer = () => {
  return (
    <footer>
      <div className="footer">
        <div className="footer__content container">
          <div className="footer__logo">
            <img src={logo} alt=""/>
          </div>
          <div className="footer__social">
            <a href="https://vk.com" target='_blank' rel="noopener noreferrer">
              <i className="fab fa-vk"></i>
            </a>
            <a href="https://telegram.com" target='_blank' rel="noopener noreferrer">
              <i className="fab fa-telegram-plane"></i>
            </a>
          </div>

          <div className="footer__copyright">
            © 2020 All Rights Reserved.
          </div>
        </div>
      </div>
    </footer>
  )
};

export default Footer;