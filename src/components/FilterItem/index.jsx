import React from "react";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from 'react-redux';

import {setModal} from "../../redux/actions/modal";
import {setMovieDetail} from "../../redux/actions/movieDetail";
import {Stars} from "../index";
import {addMovie, removeMovie} from "../../redux/actions/cart";

const FilterItem = (obj) => {
  const dispatch = useDispatch();
  const {imgUrl, name, desc, rateStars, trailer, price, id} = obj;
  const items = useSelector(({cart}) => cart.items);

  const objId = items.map(item => {
    return item.id
  });

  const watchTrailer = () => {
    dispatch(setModal(trailer));
  };

  const changeMovieDetail = () => {
    dispatch(setMovieDetail(obj));
  };

  const onAddMovie = () => {
    dispatch(addMovie(obj));
  };

  const onRemoveMovie = () => {
    dispatch(removeMovie(obj.id));
  };

  return (
    <div className="filter-content__item">
      <div className="filter-content__item-img">
        <img src={imgUrl} alt=""/>
        <div className="filter-content__item-play" onClick={watchTrailer}>
          <i className="fal fa-play-circle"></i>
        </div>
        {
          objId.indexOf(id) === -1
            ? <div className="filter-content__item-buy" onClick={onAddMovie}>
              <i className="fas fa-shopping-cart"></i>
            </div>
            : <div className="filter-content__item-buy" onClick={onRemoveMovie}>
              <i className="fas fa-trash"></i>
            </div>
        }
      </div>
      <div className="filter-content__item-info">
        <Link to='/movie-detail' className="filter-content__item-name" onClick={changeMovieDetail}>{name}</Link>
        <div className="filter-content__item-desc">{desc}</div>
        <div className="filter-content__item-footer">
          <div className="filter-content__item-rate">
            <Stars count={rateStars}/>
          </div>
          <div className="filter-content__item-price">
            {price}руб.
          </div>
        </div>
      </div>
    </div>
  )
};

export default FilterItem;