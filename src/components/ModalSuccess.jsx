import React from "react";

const ModalSuccess = ({showModal, text}) => {
  return (
    <div className='modal-success' style={{opacity: showModal ? 1 : 0, visibility: showModal ? 'visible' : 'hidden'}}>
      <div className="modal-success__content">
        <div className="modal-success__icon">
          <i className="far fa-thumbs-up"></i>
        </div>
        <div className="modal-success__title">
          {text}
        </div>
      </div>
    </div>
  )
};

export default ModalSuccess;