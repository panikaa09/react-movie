import React from "react";

import {TeamItem} from "../index";

const Team = () => {
  const teamItems = [
    {
      id: 0,
      title: 'the heart of moov',
      avatar: 'https://livedemo00.template-help.com/wt_prod-20691/images/about-2.jpg',
      name: 'Mark Johnson',
      position: 'Founder, Owner',
      desc: 'Feugiat nisl pretium fusce id velit ut tortor pretium.',
      fullDesc: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lobortis mattis aliquam faucibus purus.<br />
              Sit amet mattis vulputate enim. Fermentum leo vel orci porta non pulvinar. Velit egestas dui id ornare arcu odio ut sem. Sagittis aliquam malesuada bibendum arcu vitae elementum curabitur vitae nunc.<br />
              Bibendum arcu vitae elementum curabitur vitae nunc. Erat velit scelerisque in dictum. Tortor consequat id porta nibh venenatis cras sed. Sed egestas egestas fringilla phasellus faucibus.
            `,
      skills: [50, 20, 80, 10]
    },
    {
      id: 1,
      title: 'beauty designer',
      avatar: 'https://livedemo00.template-help.com/wt_prod-20691/images/about-3.jpg',
      name: 'Jessica Priston',
      position: 'Web Designer',
      desc: 'Sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus.',
      fullDesc: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lobortis mattis aliquam faucibus purus.<br />
              Sit amet mattis vulputate enim. Fermentum leo vel orci porta non pulvinar. Velit egestas dui id ornare arcu odio ut sem. Sagittis aliquam malesuada bibendum arcu vitae elementum curabitur vitae nunc.<br />
              Bibendum arcu vitae elementum curabitur vitae nunc. Erat velit scelerisque in dictum. Tortor consequat id porta nibh venenatis cras sed. Sed egestas egestas fringilla phasellus faucibus.
            `,
      skills: [10, 50, 30, 90]
    },
    {
      id: 2,
      title: 'progressive manager',
      avatar: 'https://livedemo00.template-help.com/wt_prod-20691/images/about-4.jpg',
      name: 'Sam Kromstain',
      position: 'Content Manager',
      desc: 'Tempor orci eu lobortis elementum nibh tellus molestie nunc.',
      fullDesc: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lobortis mattis aliquam faucibus purus.<br />
              Sit amet mattis vulputate enim. Fermentum leo vel orci porta non pulvinar. Velit egestas dui id ornare arcu odio ut sem. Sagittis aliquam malesuada bibendum arcu vitae elementum curabitur vitae nunc.<br />
              Bibendum arcu vitae elementum curabitur vitae nunc. Erat velit scelerisque in dictum. Tortor consequat id porta nibh venenatis cras sed. Sed egestas egestas fringilla phasellus faucibus.
            `,
      skills: [90, 90, 10, 40]
    },
    {
      id: 3,
      title: 'top pear manager',
      avatar: 'https://livedemo00.template-help.com/wt_prod-20691/images/about-5.jpg',
      name: 'Edna Barton',
      position: 'PR Manager',
      desc: 'Leo integer malesuada nunc vel. Eget duis at tellus at urna.',
      fullDesc: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lobortis mattis aliquam faucibus purus.<br />
              Sit amet mattis vulputate enim. Fermentum leo vel orci porta non pulvinar. Velit egestas dui id ornare arcu odio ut sem. Sagittis aliquam malesuada bibendum arcu vitae elementum curabitur vitae nunc.<br />
              Bibendum arcu vitae elementum curabitur vitae nunc. Erat velit scelerisque in dictum. Tortor consequat id porta nibh venenatis cras sed. Sed egestas egestas fringilla phasellus faucibus.
            `,
      skills: [80, 10, 50, 90]
    }
  ];

  return (
    <section className="section-team">
      <h2 className='h2-grey'>наша команда</h2>
      <div className="team__subtitle">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
        magna aliqua. Eu facilisis sed odio morbi.
      </div>
      <div className="team container">
        {
          teamItems.map(obj => (
            <TeamItem {...obj} key={obj.id}/>
          ))
        }
      </div>
    </section>
  )
};

export default Team;