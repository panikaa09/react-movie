import React from "react";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";

import {setTeamMember} from "../../redux/actions/teamMember";

const TeamItem = (obj) => {
  const dispatch = useDispatch();
  const {avatar, name, position, desc} = obj;

  const handleSetTeamMember = (obj) => {
    dispatch(setTeamMember(obj))
  };

  return (
    <Link to='/team-member' className='team__item' onClick={() => handleSetTeamMember(obj)}>
      <div className="team__avatar">
        <img src={avatar} alt=""/>
      </div>
      <div className="team__name">{name}</div>
      <div className="team__position">{position}</div>
      <div className="team__desc">{desc}</div>
    </Link>
  )
};

export default TeamItem;