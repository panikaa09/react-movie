import React from "react";

import {FactItem} from "../index";

const FactInfo = () => {
  const factItems = [
    {
      id: 0,
      number: 78,
      text: 'Distribution partners'
    },
    {
      id: 1,
      number: 23,
      text: 'Team members'
    },
    {
      id: 2,
      number: 7500,
      text: 'Daily views'
    },
    {
      id: 3,
      number: 374,
      text: 'New movies'
    }
  ];

  return (
    <section className="section-facts">
      <h2 className="h2-red">несколько фактов</h2>
      <div className="facts container">
        {
          factItems.map(obj => (
            <FactItem {...obj} key={obj.id}/>
          ))
        }
      </div>
    </section>
  )
};

export default FactInfo;