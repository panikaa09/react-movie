import React from "react";

const AboutItem = ({icon, title, text}) => {
  return (
    <div className="about__item">
      <div className="about__icon-wrap">
        <div className="about__icon">
          <i className={icon}></i>
        </div>
      </div>
      <div className="about__title">{title}</div>
      <div className="about__text">{text}</div>
    </div>
  )
};

export default AboutItem;