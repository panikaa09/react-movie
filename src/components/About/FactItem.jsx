import React from "react";
import CountUp from 'react-countup';

const FactItem = ({number, text}) => {
  return (
    <div className="facts__item">
      <CountUp className="facts__number" start={0} end={number} duration={7} />
      <div className="facts__text">{text}</div>
    </div>
  )
};

export default FactItem;