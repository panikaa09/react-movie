import React from "react";

import {AboutItem} from "../index";

const AboutInfo = () => {
  const aboutItems = [
    {
      id: 0,
      icon: 'fas fa-badge-check',
      title: 'why choose us',
      text: 'Turpis egestas integer eget aliquet nibh praesent tristique magna sit. Scelerisque mauris pellentesque pulvinar pellentesque.'
    },
    {
      id: 1,
      icon: 'fas fa-chart-line',
      title: 'our mission',
      text: 'Turpis cursus in hac habitasse platea dictumst quisque sagittis purus.'
    },
    {
      id: 2,
      icon: 'fas fa-cogs',
      title: 'what we do',
      text: 'Nulla aliquet enim tortor at auctor urna nunc. Elementum sagittis vitae et leo duis.'
    }
  ];


  return (
    <section className="section-about">
      <h2 className="h2-grey">несколько слов о нас</h2>
      <div className="about__subtitle">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
        magna aliqua<br/>
        Fermentum leo vel orci porta non pulvinar neque laoreet.
      </div>
      <div className="about container">
        {
          aboutItems.map(obj => (
            <AboutItem {...obj} key={obj.id}/>
          ))
        }
      </div>
    </section>
  )
};

export default AboutInfo;