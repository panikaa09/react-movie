import React from "react";
import YouTube from 'react-youtube';


const ModalTrailer = ({showModal, urlTrailer}) => {

  const opts = {
    height: '400',
    width: '640',
    playerVars: {
      autoplay: 1,
    },
  };

  return (
    <React.Fragment>
      {showModal &&
      <div>
        <div className='modal' style={{opacity: showModal ? '1' : '0', visibility: showModal ? 'visible' : 'hidden'}}>
          <div className="modal__content">
            <YouTube videoId={urlTrailer} opts={opts}/>
          </div>
        </div>
      </div>
      }
    </React.Fragment>
  )
};

export default ModalTrailer;