import React from "react";

const TitlePage = ({title, subtitle}) => {
  return (
    <section className="section-first-content">
      <div className="page-title__wrap">
        <div className="page-title">{title}</div>
        <div className="page-title__sub">
          {subtitle}
        </div>
      </div>
    </section>
  )
};

export default TitlePage;