import React from "react";

const Button = (props) => {

  const { text, cls } = props;
  return (
    <button className={cls} type={props.type}>{text}</button>
  )
};

export default Button;