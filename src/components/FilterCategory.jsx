import React from "react";
import classNames from 'classnames';

const FilterCategory = ({ items, activeSortType, onClickSortType }) => {

  const onSelectItem = (index) => {
    if (onClickSortType) {
      onClickSortType(index);
    }
  };

  return (
    <React.Fragment>
      {items &&
      items.map((obj, index) => (
        <button
          onClick={() => onSelectItem(obj)}
          className={
            classNames(
              "filter-nav__item btn-red-outline",
              {active: activeSortType === obj.type}
            )
          }
          key={`${obj.type}_${index}`}
          >
          {obj.name}
        </button>
      ))}
    </React.Fragment>
  )
};

export default FilterCategory;