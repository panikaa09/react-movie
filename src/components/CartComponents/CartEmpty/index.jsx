import React from "react";
import {Link} from "react-router-dom";

import shopIcon from "../../../assets/images/shop-icon.png";

const CartEmpty = () => {
  return (
    <div className="cart__empty">
      <h2 className="h2-grey">корзина пуста!</h2>
      <div className="cart__empty-subtitle">
        Кажется вы ничего не выбрали
      </div>
      <div className="cart__empty-img">
        <img src={shopIcon} alt=""/>
      </div>
      <div className="cart__empty-link">
        <Link to='/shop' className='btn-red-filled'>В магазин</Link>
      </div>
    </div>
  )
};

export default CartEmpty;