import React from "react";
import {useSelector, useDispatch} from "react-redux";

import {removeMovie} from "../../../redux/actions/cart";
import {Button} from "../../index";

const CartItems = () => {
  const dispatch = useDispatch();
  const cart = useSelector(({cart}) => cart);

  const onRemoveMovie = (id) => {
    dispatch(removeMovie(id))
  };

  return (
    <React.Fragment>
      <h2 className="h2-grey">Ваш список фильмов</h2>
      <div className='cart__container'>
        {cart.items.map((obj) => (
          <div className="cart__item" key={obj.id}>
            <div className="cart__item-img">
              <img src={obj.imgUrl} alt=""/>
            </div>
            <div className="cart__item-name">
              {obj.name}
            </div>
            <div className="cart__item-price">
              {obj.price} руб.
            </div>
            <div className="cart__item-delete" onClick={() => onRemoveMovie(obj.id)}>
              <i className="fas fa-trash"></i>
            </div>
          </div>
        ))}
        <div className="cart__total">
          <div className="cart__total-count">Всего <span>{cart.totalCount} тов.</span></div>
          <div className="cart__total-price">Общая сумма: <span>{cart.totalPrice} руб.</span></div>
        </div>
        <div className="cart__buy">
          <Button cls='btn-red-filled' text='Оплатить'/>
        </div>
      </div>
    </React.Fragment>
  )
};

export default CartItems;