import React from "react";
import {Line} from 'rc-progress';
import {useSelector} from "react-redux";

const TeamMemberSkills = () => {
  const member = useSelector(({teamMember}) => teamMember);
  const {skills} = member.teamMember;

  return (
    <section className="section-team-member-skills">
      <h2 className='h2-red'>навыки</h2>
      <div className="team-member-skills container">
        <div className="team-member-skills__item">
          <div className="team-member-skills__title">
            <span>Management</span>
            <span>{skills[0]}%</span>
          </div>
          <Line percent={skills[0]} strokeWidth="1" strokeColor="#d81a24" strokeLinecap='butt'/>
        </div>
        <div className="team-member-skills__item">
          <div className="team-member-skills__title">
            <span>Movie Production</span>
            <span>{skills[1]}%</span>
          </div>
          <Line percent={skills[1]} strokeWidth="1" strokeColor="#d81a24" strokeLinecap='butt'/>
        </div>
        <div className="team-member-skills__item">
          <div className="team-member-skills__title">
            <span>Experience</span>
            <span>{skills[2]}%</span>
          </div>
          <Line percent={skills[2]} strokeWidth="1" strokeColor="#d81a24" strokeLinecap='butt'/>
        </div>
        <div className="team-member-skills__item">
          <div className="team-member-skills__title">
            <span>Knowledge</span>
            <span>{skills[3]}%</span>
          </div>
          <Line percent={skills[3]} strokeWidth="1" strokeColor="#d81a24" strokeLinecap='butt'/>
        </div>
      </div>
    </section>
  )
};

export default TeamMemberSkills;