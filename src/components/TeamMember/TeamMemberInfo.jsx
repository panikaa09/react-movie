import React from "react";
import {useSelector} from "react-redux";

const TeamMemberInfo = () => {
  const member = useSelector(({teamMember}) => teamMember);
  const {title, avatar, name, position, fullDesc} = member.teamMember;

  return (
    <section className="section-team-member">
      <h2 className="h2-grey">{title}</h2>
      <div className="team-member container">
        <div className="team-member__avatar">
          <img src={avatar} alt=""/>
        </div>
        <div className="team-member__name">{name}</div>
        <div className="team-member__position">{position}</div>
        <div className="team-member__desc" dangerouslySetInnerHTML={{
          __html: fullDesc
        }} />
      </div>
    </section>
  )
};

export default TeamMemberInfo;