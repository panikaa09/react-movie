import React from "react";
import AwesomeSlider from 'react-awesome-slider';
import withAutoplay from 'react-awesome-slider/dist/autoplay';
import {useDispatch, useSelector} from "react-redux";
import 'react-awesome-slider/dist/styles.css';
import {Link} from "react-router-dom";

import {setModal} from "../redux/actions/modal";
import {setMovieDetail} from "../redux/actions/movieDetail";


const AutoplaySlider = withAutoplay(AwesomeSlider);

const HomeSlider = () => {
  const dispatch = useDispatch();
  const items = useSelector(({movies}) => movies.items);

  return (
    <section className='section-home-slider section-first-content'>
      <div className="home-slider container">
        <AutoplaySlider
          play={true}
          cancelOnInteraction={true}
          interval={5000}
        >
          {
            items.map((obj) => (
              <div className='home-slider__item' key={obj.id}>
                <img src={obj.imgBigUrl} alt=""/>
                <div className="home-slider__info">
                  <Link to='/movie-detail' className="home-slider__info--name" onClick={() => dispatch(setMovieDetail(obj))}>{obj.name}</Link>
                  <div className="home-slider__info--date">{obj.desc}</div>
                  <div className="home-slider__info--link btn-red" onClick={() =>  dispatch(setModal(obj.trailer))}>Смотреть трейлер</div>
                  <div className="home-slider__info--rate">
                    <div className="home-slider__info--rate-item">
                      {obj.rateKinoPoisk}
                      <i className="far fa-tape"></i>
                    </div>
                    <div className="home-slider__info--rate-item">
                      {obj.rateImdb}
                      <i className="fab fa-imdb"></i>
                    </div>
                  </div>
                </div>
              </div>
            ))
          }
        </AutoplaySlider>
      </div>
    </section>
  )
};

export default HomeSlider;