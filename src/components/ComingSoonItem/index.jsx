import React from "react";
import {useDispatch} from "react-redux";

import {setModal} from "../../redux/actions/modal";

const ComingSoonItem = ({imgBigUrl, name, desc, trailer, director, screenwriter, cast, time, descText}) => {
  const dispatch = useDispatch();

  const watchTrailer = () => {
    dispatch(setModal(trailer));
  };

  return (
    <div className="coming-soon container">
      <div className="coming-soon__img">
        <img src={imgBigUrl} alt=""/>
        <div className="coming-soon__play" onClick={watchTrailer}>
          <i className="fal fa-play-circle"></i>
        </div>
      </div>
      <div className="coming-soon__info">
        <div className="coming-soon__info-name">{name}</div>
        <div className="coming-soon__info-desc--head">
          <span>{time}</span>
          <span>|</span>
          <span>{desc}</span>
        </div>
        <div className="coming-soon__info-desc--body">
          {descText}
        </div>
        <div className="coming-soon__info-desc--footer">
          <span>Режиссёр:</span> {director} <br /><br />
          <span>Сценарист:</span> {screenwriter} <br /><br />
          <span>Роли играли:</span> {cast}
        </div>
      </div>
    </div>
  )
};

export default ComingSoonItem;