import React from "react";
import {useSelector} from "react-redux";

import {ComingSoonItem} from "./index";

const ComingSoon = () => {
  const items = useSelector(({movies}) => movies.items);

  return (
    <section className="section-coming-soon">
      <h2 className="h2-red">Скоро на сайте</h2>
      {
        items.slice(0, 1).map((obj) => (
          <ComingSoonItem key={obj.id} {...obj} />
        ))
      }
    </section>
  )
};

export default ComingSoon;