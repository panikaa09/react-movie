import React from "react";
import {useSelector, useDispatch} from 'react-redux';

import {setSortBy} from "../redux/actions/filters";

import {FilterItem, FilterCategory} from "./index";
import {fetchMoviesFilter} from "../redux/actions/movies";

const Filter = () => {
  const dispatch = useDispatch();
  const items = useSelector(({moviesFilter}) => moviesFilter.items);
  const { sortBy } = useSelector(({ filters }) => filters);
  const filterItems = [
    {name: 'Все', type: 'id', order: 'abs'},
    {name: 'Топ рейтинга', type: 'rateKinoPoisk', order: 'desc'},
    {name: 'По цене', type: 'price', order: 'abs'}
  ];

  React.useEffect(() => {
    dispatch(fetchMoviesFilter(sortBy));
  }, [sortBy, dispatch]);

  const onSelectSortType = (type) => {
    dispatch(setSortBy(type));
  };

  return (
    <section className="section-filter container">
      <h2 className='h2-grey'>
        Смотрите фильмы и сериалы онлайн
      </h2>

      <nav className="filter-nav">
        <FilterCategory
          items={filterItems}
          activeSortType={sortBy.type}
          onClickSortType={onSelectSortType}
        />
      </nav>

      <div className="filter-content">
        {items.map((obj) => (
          <FilterItem
            key={obj.id}
            {...obj}
          />
        ))
        }
      </div>
    </section>
  )
};

export default Filter;