import React from "react";

const ContactsItem = ({icon, title, info}) => {
  return (
    <div className='contacts__item'>
      <div className="contacts__icon">
        <i className={icon}></i>
      </div>
      <div className="contacts__title">{title}</div>
      {
        info.map(({tag, text}) => {
          switch (tag) {
            case 'link-phone':
              return text.map((text, index) => (
                <a className='contacts__link' href={`tel:${text.replace(/[^0-9+]/g, '')}`} key={'info' + index}>{text}</a>
              ));

            case 'html':
              return text.map((text, index) => (
                <div className='contacts__text' key={'info' + index}>{text}</div>
              ));

            case 'link-email':
              return text.map((text, index) => (
                <a className='contacts__link' href={`mailto:${text}`} key={'info' + index}>{text}</a>
              ));

            case 'link-web':
              return text.map((text, index) => (
                <a className='contacts__link' href={text} target='_blank' rel="noopener noreferrer" key={'info' + index}>{text}</a>
              ));

            default:
              return null;
          }
        })
      }
    </div>
  )
};

export default ContactsItem;