import React from "react";

import {ContactsItem} from "../index";

const ContactsInfo = () => {
  const contactsItems = [
    {
      id: 0,
      icon: 'fas fa-phone-alt',
      title: 'Contact',
      info: [{ tag: 'link-phone', text: ['+91-123456789', '+91-4444-5555']}]
    },
    {
      id: 1,
      icon: 'fas fa-map-marker-alt',
      title: 'Location',
      info: [{ tag: 'html', text: ['601 - Ram Nagar , India', 'Omex City 245 , America']}]
    },
    {
      id: 2,
      icon: 'fas fa-envelope',
      title: 'Email',
      info: [{ tag: 'link-email', text: ['presenter@example.com', 'movie@example.com']}]
    },
    {
      id: 3,
      icon: 'fas fa-globe',
      title: 'Web Address',
      info: [{ tag: 'link-web', text: ['www.example.com']}]
    }
  ];
  return (
    <section className="section-contacts">
      <h2 className="h2-grey">Наши контакты</h2>
      <div className="contacts container">
        {
          contactsItems.map(obj => (
            <ContactsItem {...obj} key={obj.id}/>
          ))
        }
      </div>
    </section>
  )
};

export default ContactsInfo;