import React from "react";
import {YMaps, Map, Placemark} from 'react-yandex-maps';

import myIcon from '../../assets/images/pin.png';

const ContactsMap = () => {
  return (
    <section className="section-contacts-map">
      <div className="contacts-map">
        <YMaps>
          <Map defaultState={{center: [55.75, 37.57], zoom: 9}} width={100+'%'} height={460}>
            <Placemark
              geometry={[55.75, 37.57]}
              options={{
                iconLayout: 'default#image',
                iconImageHref: myIcon,
                iconImageSize: [40, 61],
                iconImageOffset: [-50, -18],
              }}
            />
          </Map>
        </YMaps>
      </div>
    </section>
  )
};

export default ContactsMap;