import React from "react";
import {useDispatch} from "react-redux";
import {useForm} from "react-hook-form";

import {setShowSuccess} from "../../redux/actions/modal";
import Button from "../Button";

const ContactsForm = () => {
  const dispatch = useDispatch();
  const {register, handleSubmit, errors} = useForm();
  const onSubmit = (data) => {
    console.log(data);
    dispatch(setShowSuccess('Спасибо за сообщение! В скором времени мы с Вами свяжемся'));
    document.body.style.overflow = 'hidden';
  };

  return (
    <section className="section-contacts-form">
      <h2 className="h2-grey">обратная связь</h2>
      <div className="contacts-form container">
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="contacts-form__form-content">
            <div className="contacts-form__row">
              <div className='form-row form-row--white'>
                <input name='firstName' placeholder='Введите имя' ref={register({required: true, maxLength: 80})}
                       style={{
                         borderColor: errors.firstName ? '#d81a24' : '',
                         borderWidth: errors.firstName ? '1px 1px 1px 10px' : ''
                        }}
                />
              </div>
              <div className='form-row form-row--white'>
                <input name='lastName' placeholder='Введите фамилию' ref={register({required: true, maxLength: 80})}
                       style={{
                         borderColor: errors.lastName ? '#d81a24' : '',
                         borderWidth: errors.lastName ? '1px 1px 1px 10px' : ''
                       }}
                />
              </div>
            </div>
            <div className="contacts-form__row">
              <div className='form-row form-row--white'>
                <input name='email' placeholder='Введите email' ref={register({
                  required: "Required",
                  pattern: {
                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                    message: "invalid email address"
                  }
                })}
                       style={{
                         borderColor: errors.email ? '#d81a24' : '',
                         borderWidth: errors.email ? '1px 1px 1px 10px' : ''
                       }}
                />
              </div>
              <div className='form-row form-row--white'>
                <input name='mobile' placeholder='Введите номер телефона' ref={register({required: true, minLength: 6, maxLength: 12})}
                       style={{
                         borderColor: errors.mobile ? '#d81a24' : '',
                         borderWidth: errors.mobile ? '1px 1px 1px 10px' : ''
                       }}
                />
              </div>
            </div>
            <div className="contacts-form__row">
              <div className="form-row__textarea">
                <textarea name="message" rows="10" ref={register({required: true, maxLength: 80})} placeholder='Введите сообщение'
                          style={{
                            borderColor: errors.message ? '#d81a24' : '',
                            borderWidth: errors.message ? '1px 1px 1px 10px' : ''
                          }}
                />
              </div>
            </div>
            <div className="contacts-form__btn">
              <Button text='Отправить' cls='btn-red-filled' type='submit'/>
            </div>
          </div>
        </form>
      </div>
    </section>
  )
};

export default ContactsForm;