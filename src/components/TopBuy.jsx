import React from "react";
import {useSelector} from "react-redux";

import {TopBuyItem} from "./index";


const TopBuy = () => {
  const items = useSelector(({movies}) => movies.items);

  return (
    <section className="section-top-buy container">
      <h2 className='h2-grey'>Самые популярные</h2>
      <div className="top-buy">
        {
          items.slice(4, 8).map((obj) => (
            <TopBuyItem key={obj.id} {...obj}/>
          ))
        }
      </div>
    </section>
  )
};

export default TopBuy;