import React from "react";
import {useDispatch, useSelector} from "react-redux";

import {setModal} from "../../redux/actions/modal";
import {Stars} from "../index";
import {addMovie, removeMovie} from "../../redux/actions/cart";

const MovieDetailItem = ({detail}) => {
  const dispatch = useDispatch();
  const {id, name, trailer, imgUrl, desc, time, descText, price, imgBigUrl, rateStars} = detail;
  const items = useSelector(({cart}) => cart.items);

  const objId = items.map(item => {
    return item.id
  });

  const watchTrailer = () => {
    dispatch(setModal(trailer));
  };

  const onAddMovie = () => {
    dispatch(addMovie(detail));
  };

  const onRemoveMovie = () => {
    dispatch(removeMovie(detail.id));
  };

  return (
    <React.Fragment>
      { id &&
        <section className="section-movie-detail-item container">
          <h2 className="h2-grey">
            {name}
          </h2>
          <div className="movie-detail-item">
            <div className="movie-detail-item__column">
              <div className="movie-detail-item__img">
                <img src={imgUrl} alt=""/>
              </div>
              <div className="movie-detail-item__info">
                <div className="movie-detail-item__info-rate">
                  <Stars count={rateStars}/>
                </div>
                <div className="movie-detail-item__info-desc--head">
                  {desc}
                </div>
                <div className="movie-detail-item__info-desc--head">
                  {time}
                </div>
                <div className="movie-detail-item__info-desc--body">
                  {descText}
                </div>
                <div className="movie-detail-item__info-desc--footer">
                  <div className="movie-detail-item__info-price">
                    {price}руб.
                  </div>
                  {
                    objId.indexOf(id) === -1
                      ? <div className="movie-detail-item__info-cart" onClick={onAddMovie}>
                        <i className="fas fa-shopping-cart"></i>
                      </div>
                      : <div className="movie-detail-item__info-cart" onClick={onRemoveMovie}>
                        <i className="fas fa-trash"></i>
                      </div>
                  }
                </div>
              </div>
            </div>
            <div className="movie-detail-item__column">
              <div className="movie-detail-item__trailer">
                <img src={imgBigUrl} alt=""/>
                <div className="movie-detail-item__trailer-play" onClick={watchTrailer}>
                  <i className="fal fa-play-circle"></i>
                </div>
              </div>
            </div>
          </div>
        </section>
      }
    </React.Fragment>
  )
};

export default MovieDetailItem;