import React from "react";

const GenresItem = ({icon, title, text}) => {
  return (
    <div className="genres__item">
      <div className="genres__header">
        <div className="genres__icon">
          <i className={icon}></i>
        </div>
        <div className="genres__title">
          {title}
        </div>
      </div>
      <div className="genres__body">
        {text}
      </div>
    </div>
  )
};

export default GenresItem;