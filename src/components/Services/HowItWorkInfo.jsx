import React from "react";

import {HowItWorkItem} from "../index";

const HowItWorkInfo = () => {
  const howItWorkItems = [
    {
      id: 0,
      icon: 'fas fa-user',
      title: `create<br/>an account`,
      text: 'Sodales ut eu sem integer vitae justo eget. Sem nulla pharetra diam sit amet.'
    },
    {
      id: 1,
      icon: 'far fa-money-bill-alt',
      title: `choose<br />your plan`,
      text: 'Enim nec dui nunc mattis enim ut tellus elementum. Velit euismod in pellentesque massa placerat.'
    },
    {
      id: 2,
      icon: 'fas fa-thumbs-up',
      title: `select your<br/>film or tv show`,
      text: 'Aliquam eleifend mi in nulla posuere sollicitudin aliquam ultrices.'
    },
    {
      id: 3,
      icon: 'fas fa-film',
      title: `enjoy and<br/>rate movies`,
      text: 'Euismod quis viverra nibh cras pulvinar. Magna fringilla urna porttitor rhoncus dolor purus non.'
    }
  ];

  return (
    <section className="section-how-it-works">
      <h2 className="h2-red">как это работает</h2>
      <div className="how-it-works container">
        {
          howItWorkItems.map(obj => (
            <HowItWorkItem {...obj} key={obj.id}/>
          ))
        }
      </div>
    </section>
  )
};

export default HowItWorkInfo;