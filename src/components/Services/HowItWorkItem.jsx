import React from "react";

const HowItWorkItem = ({icon, title, text}) => {
  return (
    <div className="how-it-works__item">
      <div className="how-it-works__icon-wrap">
        <div className="how-it-works__icon">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <div className="how-it-works__icon-content">
            <i className={icon}></i>
          </div>
        </div>
      </div>
      <div className="how-it-works__title"  dangerouslySetInnerHTML={{
        __html: title
      }}>
      </div>
      <div className="how-it-works__text">
        {text}
      </div>
    </div>
  )
};

export default HowItWorkItem;