import React from "react";

import {ServicesItem} from "../index";
import img from "../../assets/images/services.jpg";

const ServicesInfo = () => {
  const servicesItems = [
    {
      id: 0,
      icon: 'fal fa-desktop-alt',
      title: 'Ultra HD Quality',
      text: 'Ullamcorper velit sed ullamcorper morbi tincidunt ornare.'
    },
    {
      id: 1,
      icon: 'fal fa-film-alt',
      title: 'Wide Range of Movies',
      text: 'Ultrices dui sapien eget mi proin sed libero.'
    },
    {id: 2, icon: 'fal fa-bell', title: 'Exclusive Releases', text: 'Mauris vitae ultricies leo integer.'},
    {
      id: 3,
      icon: 'fal fa-rocket',
      title: 'Global Availability',
      text: 'Tempor orci eu lobortis elementum nibh tellus molestie nunc.'
    }
  ];

  return (
    <section className="section-services">
      <h2 className="h2-grey">что мы предлагаем</h2>
      <div className="services container">
        <div className="services__img">
          <img src={img} alt=""/>
        </div>
        <div className="services__info">
          <div className="services__info-head">
            Id donec ultrices tincidunt arcu. Vulputate enim nulla aliquet porttitor lacus luctus. Sit amet cursus sit
            amet dictum sit amet justo donec. Nec ullamcorper sit amet risus nullam eget felis.
          </div>
          <div className="services__info-body">
            {
              servicesItems.map(obj => (
                <ServicesItem {...obj} key={obj.id}/>
              ))
            }
          </div>
        </div>
      </div>
    </section>
  )
};

export default ServicesInfo;