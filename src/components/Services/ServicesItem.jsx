import React from "react";

const ServicesItem = ({icon, title, text}) => {

  return (
    <div className="services__info-item">
      <div className="services__info-item--head">
        <div className="services__info-item--icon">
          <i className={icon}></i>
        </div>
        <div className="services__info-item--title">{title}</div>
      </div>
      <div className="services__info-item--body">{text}</div>
    </div>
  )
};

export default ServicesItem;