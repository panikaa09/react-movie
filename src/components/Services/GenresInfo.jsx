import React from "react";

import {GenresItem} from "../index";

const GenresInfo = () => {
  const genresItems = [
    {
      id: 0,
      icon: 'fas fa-cloud',
      title: 'Fantasy',
      text: 'Venenatis lectus magna fringilla urna porttitor rhoncus dolor. Amet purus gravida quis blandit. Sed enim ut sem viverra aliquet eget. Tristique senectus et netus et.'
    },
    {
      id: 1,
      icon: 'fas fa-bolt',
      title: 'Action movies',
      text: 'Pellentesque eu tincidunt tortor aliquam nulla facilisi cras. Gravida in fermentum et sollicitudin ac orci phasellus. Donec pretium vulputate sapien nec sagittis aliquam malesuada bibendum.'
    },
    {
      id: 2,
      icon: 'fas fa-star',
      title: 'Crime',
      text: 'Adipiscing elit ut aliquam purus sit amet luctus venenatis. Pellentesque elit eget gravida cum. Enim ut tellus elementum sagittis vitae et leo.'
    },
    {
      id: 3,
      icon: 'fas fa-user',
      title: 'Biographical',
      text: 'Etiam dignissim diam quis enim lobortis scelerisque fermentum. Ipsum dolor sit amet consectetur.'
    },
    {
      id: 4,
      icon: 'fas fa-fighter-jet',
      title: 'Military',
      text: 'Vel turpis nunc eget lorem dolor sed viverra ipsum. Tortor pretium viverra suspendisse potenti nullam ac tortor vitae. Facilisi cras fermentum odio eu feugiat pretium nibh ipsum consequat.'
    },
    {
      id: 5,
      icon: 'fab fa-500px',
      title: 'Adventure',
      text: 'olutpat est velit egestas dui id ornare arcu odio. Vitae turpis massa sed elementum tempus.'
    }
  ];

  return (
    <section className="section-genres">
      <h2 className="h2-grey">жанры фильмов</h2>
      <div className="genres container">
        {
          genresItems.map(obj => (
            <GenresItem {...obj} key={obj.id}/>
          ))
        }
      </div>
    </section>
  )
};

export default GenresInfo;