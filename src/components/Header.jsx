import React from "react";
import {NavLink} from 'react-router-dom';
import {useSelector} from "react-redux";

import logo from '../assets/images/logo.png';

const Header = () => {
  const cartCount = useSelector(({cart}) => cart.items.length);
  const [scrolled, setScrolled] = React.useState(false);

  React.useEffect(() => {
    window.addEventListener('scroll', () => {
      if (window.scrollY === 0 && scrolled === true) {
        setScrolled(!scrolled);
      } else if (window.scrollY !== 0 && scrolled !== true) {
        setScrolled(!scrolled);
      }
    });
  }, [scrolled]);

  return (
    <header>
      <div className="header"
           style={{
             background: scrolled ? '#151515' : '',
             boxShadow: scrolled ? '1px 4px 5px 2px rgba(0, 0, 0, 0.11)' : ''
           }}
      >
        <div className="header__content container">
          <div className="header__left">
            <div className="header__logo">
              <img src={logo} alt=""/>
            </div>
          </div>
          <div className="header__right">
            <nav className="header__nav">
              <ul className="header__nav-list">
                <li className='header__nav-parent-item'>
                  <NavLink to="/" className='header__nav-link--parent' activeClassName="active" exact>
                    Главная
                  </NavLink>
                </li>
                <li className='header__nav-parent-item'>
                  <NavLink to="/info" className='header__nav-link--parent'

                  >
                    Инфо
                  </NavLink>
                  <ul className="header__nav-list-more">
                    <li>
                      <NavLink to="/info/services" className='header__nav-link--children' activeClassName="active">
                        Услуги
                      </NavLink>
                    </li>
                    <li>
                      <NavLink to="/info/about" className='header__nav-link--children' activeClassName="active">
                        О нас
                      </NavLink>
                    </li>
                    <li>
                      <NavLink to="/info/faq" className='header__nav-link--children' activeClassName="active">
                        Частые вопросы
                      </NavLink>
                    </li>
                  </ul>
                </li>
                <li className='header__nav-parent-item'>
                  <NavLink to="/shop" className='header__nav-link--parent' activeClassName="active">
                    Магазин
                  </NavLink>
                </li>
                <li className='header__nav-parent-item'>
                  <NavLink to="/contacts" className='header__nav-link--parent' activeClassName="active">
                    Контакты
                  </NavLink>
                </li>
                <li className='header__nav-parent-item'>
                  <NavLink to="/cart" className='header__nav-link--parent' activeClassName="active">
                    <i className="fas fa-shopping-cart"></i>
                    <span>
                      {
                        cartCount > 0 ? cartCount : null
                      }
                    </span>
                  </NavLink>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;